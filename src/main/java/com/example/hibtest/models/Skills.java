package com.example.hibtest.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Skills {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(name = "teacher_skills", joinColumns = @JoinColumn(name = "skills_id"),
    inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    public List<Teacher> teacher;

    @JsonGetter("teacher")
    public List<String> teachers() {
        if (teacher != null) {
            return teacher.stream()
                    .map(teacher -> "api/v1/teachers/" + teacher.getId()).collect(Collectors.toList());
        }
        return null;

    }
}
