package com.example.hibtest.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    String firstName;
    String lastName;


    @ManyToMany
    @JoinTable(name = "teacher_skills", joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "skills_id"))
    public List<Skills> skills;

    @JsonGetter("skills")
    public List<String> skills(){
        if(skills != null){
            return skills.stream()
                    .map(skills -> "api/v1/skills/" + skills.getId()).collect(Collectors.toList());
        }
        return null;
    }



    @OneToMany
    @JoinColumn(name = "teacher_id")
    List<Student> students;

    @JsonGetter("students")
    public List<String> students(){
        if(students != null){
            return students.stream()
                    .map(student -> "api/v1/students/" + student.getId()).collect(Collectors.toList());
        }
        return null;
    }
}
