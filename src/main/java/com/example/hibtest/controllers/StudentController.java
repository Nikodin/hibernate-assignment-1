package com.example.hibtest.controllers;

import com.example.hibtest.models.Student;
import com.example.hibtest.repo.StudentRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/students")
@Tag(name = "Student", description = "Contains Student Objects as: \n Long id, \n String firstName \n String lastName \n and a @ManyToMany teacher relationship, bidirectional linked")
public class StudentController {

    @Autowired
    StudentRepo studentRepo;

    @Operation(summary = "Get all students")
    @GetMapping()
    public ResponseEntity<List<Student>> getAllStudents() {
        List<Student> students = studentRepo.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(students,status);
    }

    @Operation(summary = "Get specific student by ID")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable Long id){
        Student respStudent = new Student();
        HttpStatus status;
        //Checks if id exists
        if(studentRepo.existsById(id)) {
            status = HttpStatus.OK;
            respStudent = studentRepo.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(respStudent, status);
    }

    @Operation(summary = "Update specific Student by ID, matching RequestBody")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student){
        Student respStudent = new Student();
        HttpStatus status;
        if (!id.equals(student.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(respStudent, status);
        }
        respStudent = studentRepo.save(student);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(respStudent, status);
    }

    @Operation(summary = "Add a new Student, matching RequestBody")
    @PostMapping
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        Student respStudent = studentRepo.save(student);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(respStudent, status);
    }

    @Operation(summary = "Delete target Student by ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Long id){
        if (!studentRepo.existsById(id)) {
            return new ResponseEntity<>("No such Student exists!", HttpStatus.BAD_REQUEST);
        }
        studentRepo.deleteById(id);
        return new ResponseEntity<>("Student data with ID: " + id + "has been deleted!", HttpStatus.ACCEPTED);
    }
}
