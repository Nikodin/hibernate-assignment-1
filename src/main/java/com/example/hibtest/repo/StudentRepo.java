package com.example.hibtest.repo;

import com.example.hibtest.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    Boolean existsByFirstNameAndLastName(String firstName, String lastName);
}
