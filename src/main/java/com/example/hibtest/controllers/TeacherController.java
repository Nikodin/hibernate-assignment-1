package com.example.hibtest.controllers;

import com.example.hibtest.models.Teacher;
import com.example.hibtest.repo.TeacherRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/teachers")
@Tag(name = "Teacher", description = "Contains Teacher Objects as: \n Long id, \n String firstName \n String lastName \n and a @ManyToMany Skills relationship, bidirectional linked")
public class TeacherController {

        @Autowired
        TeacherRepo teacherRepo;

        @Operation(summary = "Get all Teachers")
        @GetMapping()
        public ResponseEntity<List<Teacher>>getAllTeachers() {
            List<Teacher> teachers = teacherRepo.findAll();
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(teachers,status);
        }

        @Operation(summary = "Get target Teacher by ID")
        @GetMapping("/{id}")
        public ResponseEntity<Teacher> getTeacher(@PathVariable Long id){
            Teacher respTeacher = new Teacher();
            HttpStatus status;
            //Checks if id exists
            if(teacherRepo.existsById(id)) {
                status = HttpStatus.OK;
                respTeacher = teacherRepo.findById(id).get();
            } else {
                status = HttpStatus.NOT_FOUND;
            }
            return new ResponseEntity<>(respTeacher, status);
        }

        @Operation(summary = "Update a Teacher by ID, matching RequestBody")
        @PutMapping("/{id}")
        public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher teacher){
            Teacher respStudent = new Teacher();
            HttpStatus status;
            if (!id.equals(teacher.getId())){
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(respStudent, status);
            }
            respStudent = teacherRepo.save(teacher);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(respStudent, status);
        }

        @Operation(summary = "Add a new Teacher, matching RequestBody")
        @PostMapping
        public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher){
            Teacher respStudent = teacherRepo.save(teacher);
            HttpStatus status = HttpStatus.CREATED;
            return new ResponseEntity<>(respStudent, status);
        }


        @Operation(summary = "Delete specific Teacher by ID")
        @DeleteMapping("/{id}")
        public ResponseEntity<String> deleteTeacher(@PathVariable Long id){
            if (!teacherRepo.existsById(id)) {
                return new ResponseEntity<>("No such Teacher exists!", HttpStatus.BAD_REQUEST);
            }
            teacherRepo.deleteById(id);
            return new ResponseEntity<>("Teacher data with ID: " + id + "has been deleted!", HttpStatus.ACCEPTED);

        }
    }

