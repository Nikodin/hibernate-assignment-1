package com.example.hibtest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
        info = @Info(
                title = "School API",
                description = "Students, Teachers and Skills"))
@SpringBootApplication
public class HibTestApplication {


    public static void main(String[] args) {
        SpringApplication.run(HibTestApplication.class, args);
    }

}
