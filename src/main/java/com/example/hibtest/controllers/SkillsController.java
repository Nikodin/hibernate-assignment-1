package com.example.hibtest.controllers;

import com.example.hibtest.models.Skills;
import com.example.hibtest.repo.SkillsRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

    @RestController
    @CrossOrigin(origins = "*")
    @RequestMapping("/api/v1/skills")
    @Tag(name = "Skills", description = "Contains Skill Objects as: \n Long id, \n String description \n and a @ManyToOne Teacher relationship, with Teacher")
    public class SkillsController {

        @Autowired
        SkillsRepo skillsRepo;

        @Operation(summary = "Get all Skills")
        @GetMapping()
        public ResponseEntity<List<Skills>> getAllStudents() {
            List<Skills> skills = skillsRepo.findAll();
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(skills,status);
        }

        @Operation(summary = "Get a specific skill by target ID")
        @GetMapping("/{id}")
        public ResponseEntity<Skills> getSkill(@PathVariable Long id){
            Skills respSkills = new Skills();
            HttpStatus status;
            //Checks if id exists
            if(skillsRepo.existsById(id)) {
                status = HttpStatus.OK;
                respSkills = skillsRepo.findById(id).get();
            } else {
                status = HttpStatus.NOT_FOUND;
            }
            return new ResponseEntity<>(respSkills, status);
        }

        @Operation(summary = "Update specific skill by target ID")
        @PutMapping("/{id}")
        public ResponseEntity<Skills> updateSkill(@PathVariable Long id, @RequestBody Skills skills){
            Skills respSkills = new Skills();
            HttpStatus status;
            if (!id.equals(skills.getId())){
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(respSkills, status);
            }
            respSkills = skillsRepo.save(skills);
            status = HttpStatus.OK;
            return new ResponseEntity<>(respSkills, status);
        }

        @Operation(summary = "Add a new Skill, post matching RequestBody")
        @PostMapping
        public ResponseEntity<Skills> addSkill(@RequestBody Skills skills){
            Skills respStudent = skillsRepo.save(skills);
            HttpStatus status = HttpStatus.CREATED;
            return new ResponseEntity<>(respStudent, status);
        }


        @Operation(summary = "Delete a specific Skill by target ID")
        @DeleteMapping("/{id}")
        public ResponseEntity<String> deleteSkill(@PathVariable Long id){
            if (!skillsRepo.existsById(id)) {
                return new ResponseEntity<>("No such Skill exists!", HttpStatus.BAD_REQUEST);
            }
            skillsRepo.deleteById(id);
            return new ResponseEntity<>("Skill data with ID: " + id + "has been deleted!", HttpStatus.ACCEPTED);
        }
    }

