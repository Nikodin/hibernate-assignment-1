package com.example.hibtest.repo;


import com.example.hibtest.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepo extends JpaRepository<Teacher, Long> {
    Boolean existsByFirstNameAndLastName(String firstName, String lastName);
}

